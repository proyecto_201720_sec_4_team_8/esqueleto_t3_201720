package model.data_structures;

import java.util.Iterator;

public class Cola<T> implements Iterable<T>
{
	private T[] arreglo = (T[]) new Object[2];
	private int n = 0;
	private int primero = 0;
	private int ultimo = 0;
	
	private class ListIterator implements Iterator<T>
	{
		private int actual = 0;
		
		public boolean hasNext()
		{
			return actual < n;
		}
		
		public T next()
		{
			T objeto = arreglo[(actual + primero) % arreglo.length];
			actual++;
			return objeto;
		}
	}
	
	public boolean isEmpty()
	{
		return n == 0;
	}
	
	public T get(int i)
	{
		return arreglo[i];
	}
	
	public int size()
	{
		return n;
	}
	
	public T peek()
	{
		return arreglo[primero];
	}
	
	public void resize(int capacidad)
	{
		assert capacidad >= n;
		T[] temp = (T[]) new Object[capacidad];
		for(int i = 0; i < n; i++)
		{
			temp[i] = arreglo[(primero + i) % arreglo.length];
		}
		arreglo = temp;
		primero = 0;
		ultimo = n;
	}
	
	public void enqueue(T agregar)
	{
		if(n == arreglo.length)
		{
			resize(2 * arreglo.length);
		}
		arreglo[ultimo++] = agregar;
		if(ultimo == arreglo.length)
		{
			ultimo = 0;
		}
		n++;
	}
	
	public T dequeue()
	{
		T item = arreglo[primero];
		arreglo[primero] = null;
		n--;
		primero++;
		if(primero == arreglo.length)
		{
			primero = 0;
		}
		if(n > 0 && n == arreglo.length/4)
		{
			resize(arreglo.length/2);
		}
		return item;
	}
	
	public Iterator<T> iterator()
	{
		return new ListIterator();
	}
	
}