public class STSManager implements ISTSManager
{
	private Cola<StopVO> listaParadas;

	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		return null;
	}

	public void loadStops()
	{
			String csvFile = "data/stops.txt";
			BufferedReader br = null;
			String line = "";
			try
			{
				Cola<StopVO> lista = new Cola<StopVO>();
				br = new BufferedReader(new FileReader(csvFile));
				while ((line = br.readLine()) != null)
				{
					String[] datos = line.split(",");
					if(!(datos[0].equals("stop_id")))
					{
						StopVO agregar = new StopVO(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6]);
						lista.enqueue(agregar);
					}	
				}
				listaParadas = lista;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally 
			{
				if (br != null)
				{
					try
					{
						br.close();
					}
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}
			}
		}			
	}
