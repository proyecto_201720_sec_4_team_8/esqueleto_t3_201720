package model.logic;

import model.exceptions.TripNotFoundException;
import java.io.FileReader;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import java.util.Map.Entry;


public class Lee_Jason 
{
	public static void main(String[] args) {

		JSONParser parser = new JSONParser();

		Cola colaNueva = new Cola();

		try {
			for(int i =0; i<1000;i++)
			{
				Object obj = parser.parse(new FileReader("data/BUSES_SERVICE_"+i+".json"));

				JSONObject jsonObject = (JSONObject) obj;

				String latitude = (String) jsonObject.get("Latitude");

				String longitude = (String) jsonObject.get("Longitude");

				String vehicleNumber = (String) jsonObject.get("VehicleNo");

				String routeNumber =  (String) jsonObject.get("RouteNo");

				String tripId = (String) jsonObject.get("TripId");

				BusUpdateVO nuevo = new BusUpdateVo (vehicleNumber, tripId, routeNumber, latitude, longitude );
				
				cola.enqueue(nuevo);
			}
		} 
		catch (FileNotFoundException e)

		{

			throw new TripNotFoundException();
		} 

		catch (IOException e) 

		{
			throw new TripNotFoundException();

		} 

		catch (ParseException e) 

		{

			throw new TripNotFoundException();
		}

	}

}

