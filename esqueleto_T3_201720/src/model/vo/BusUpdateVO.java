public class BusUpdateVO
{
	private String vehicleNO;
	private String tripID;
	private String routeNO;
	private String lat;
	private String lon;
	
	public BusUpdateVO(String pVehicleNO, String pTripID, String pRouteNO, String pLat, String pLon)
	{
		vehicleNO = pVehicleNO;
		tripID = pTripID;
		routeNO = pRouteNO;
		lat = pLat;
		lon = pLon;
	}
	
	public String darVehicleNO()
	{
		return vehicleNO;
	}
	
	public String darTripID()
	{
		return tripID;
	}
	
	public String darRouteNO()
	{
		return routeNO;
	}
	
	public String darLat()
	{
		return lat;
	}
	
	public String darLon()
	{
		return lon;
	}
}